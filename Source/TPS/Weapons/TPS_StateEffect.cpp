// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS_StateEffect.h"
#include "TPS/Character/TPSHealthComponent.h"
#include "TPS/Interface/TPS_IGameActor.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"


bool UTPS_StateEffect::InitObject(AActor* Actor)
{
	UE_LOG(LogTemp, Warning, TEXT("UTPS_StateEffect::InitObject"));

	myActor = Actor;

	ITPS_IGameActor* myInterface = Cast<ITPS_IGameActor>(myActor);
	if (myInterface) 
	{
		myInterface->Execute_AddEffect(myActor, this);
	}

	return true;
}

void UTPS_StateEffect::DestroyObject()
{

	ITPS_IGameActor* myInterface = Cast<ITPS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->Execute_RemoveEffect( myActor, this);
	}

	myActor = nullptr;

	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
}





bool UTPS_StateEffect_ExecuteOnce::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);
	ExecuteOnce();

	return true;
}

void UTPS_StateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UTPS_StateEffect_ExecuteOnce::ExecuteOnce()
{
	if (myActor)
	{
		UTPSHealthComponent * myHealthComp = Cast<UTPSHealthComponent>( myActor->GetComponentByClass(UTPSHealthComponent::StaticClass()) );
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue_OnServer(Power);
		}
	}

	DestroyObject();
}


bool UTPS_StateEffect_ExecuteTimer::InitObject(AActor* Actor)
{
	UE_LOG(LogTemp, Warning, TEXT("UTPS_StateEffect_ExecuteTimer::InitObject"));
	Super::InitObject(Actor);

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UTPS_StateEffect_ExecuteTimer::DestroyObject, Timer, false);
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UTPS_StateEffect_ExecuteTimer::Execute, RateTime, true);
	}

	//28.1 10:24 emitter spawning function removed. Seems to be everything in this scope should be removed.
	//if (ParticleEffect)
	//{
		//FName NameBoneToAttached;
		//FVector Loc = FVector(0);

		//myActor = Actor;
		//ITPS_IGameActor* myInterface = Cast<ITPS_IGameActor>(myActor);
		//if (myInterface)
		//{
		//	NameBoneToAttached = myInterface->GetEffectBone();
		//	Loc = myInterface->GetEffectOffset();
		//}

		//ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(), NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
	//}

	return true;
}

void UTPS_StateEffect_ExecuteTimer::DestroyObject()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
	}

	// Removed in 28.1 26:40. OM.
	//UE_LOG(LogTemp, Warning, TEXT("UTPS_StateEffect_ExecuteTimer::DestroyObject"));
	//if (ParticleEmitter)
	//{
	//	ParticleEmitter->DestroyComponent();
	//	ParticleEmitter = nullptr;
	//}

	Super::DestroyObject();
}

void UTPS_StateEffect_ExecuteTimer::Execute()
{
	if (myActor)
	{
		UTPSHealthComponent* myHealthComp = Cast<UTPSHealthComponent>(myActor->GetComponentByClass(UTPSHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue_OnServer(Power);
		}
	}
}

void UTPS_StateEffect::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UTPS_StateEffect, NameBone);
}
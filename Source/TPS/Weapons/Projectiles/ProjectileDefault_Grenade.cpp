// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault_Grenade.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

int32 ShowExplodeDebugSphere = 0;
FAutoConsoleVariableRef ConVarShowExplodeSphere(TEXT("TPS.DebugExplode"), ShowExplodeDebugSphere, TEXT("Shows a debug sphere around explosion."),ECVF_Cheat);

void AProjectileDefault_Grenade::BeginPlay()
{
	Super::BeginPlay();

}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplose(DeltaTime);


}

void AProjectileDefault_Grenade::TimerExplose(float DeltaTime)
{

	if (TimerEnabled)
	{
		if (TimerToExplose > ProjectileSetting.TimeToExplode)
		{
			//Explose
			//UE_LOG(LogTemp, Warning, TEXT("Grenade: Boom!"));
			Explose();

		}
		else
		{
			TimerToExplose += DeltaTime;
		}
	}
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (TimerEnabled == false)
	{
		Explose();
	}

	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileDefault_Grenade::ImpactProjectile()
{
	//Init Grenade
	if (TimerEnabled == false)
	{	
		TimerEnabled = true;
		//UE_LOG(LogTemp, Warning, TEXT("Grenade: Tick-tick."));
	}
}

void AProjectileDefault_Grenade::Explose()
{
	if (ShowExplodeDebugSphere)
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ExplodeMaxRadiusDamage, 32, FColor::Orange, false, 1.0f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ExplodeMinRadiusDamage, 32, FColor::Red, false, 1.0f);
	}

	TimerEnabled = false;
	if (ProjectileSetting.ExploseFX)
	{
		//UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSetting.ExploseFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
		SpawnExplosionFX_Multicast(ProjectileSetting.ExploseFX, GetActorLocation());
	}
	if (ProjectileSetting.ExploseSound)
	{
		//UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.ExploseSound, GetActorLocation());
		SpawnExplosionSound_Multicast(ProjectileSetting.ExploseSound, GetActorLocation());
	}

	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ProjectileSetting.ExploseMaxDamage,
		ProjectileSetting.ExploseMaxDamage * 0.2f,
		GetActorLocation(),
		ProjectileSetting.ExplodeMinRadiusDamage,
		ProjectileSetting.ExplodeMaxRadiusDamage,
		ProjectileSetting.ExplodeFalloff,
		NULL, IgnoredActor, this, nullptr);


	// If change DamageCauser from "this" to "nullptr" the character receive no damage. 

	this->Destroy();
	//UE_LOG(LogTemp, Warning, TEXT("Grenade: Boom!"));
}

void AProjectileDefault_Grenade::SpawnExplosionFX_Multicast_Implementation(UParticleSystem* Fx, FVector Location)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), Fx, Location, FRotator(), FVector(1.0f));
}

void AProjectileDefault_Grenade::SpawnExplosionSound_Multicast_Implementation(USoundBase* Sound, FVector Location)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), Sound, Location);
}



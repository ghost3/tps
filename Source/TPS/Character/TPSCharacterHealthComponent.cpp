// Fill out your copyright notice in the Description page of Project Settings.


#include "TPSCharacterHealthComponent.h"

void UTPSCharacterHealthComponent::ChangeHealthValue_OnServer(float ChangeValue)
{
	float CurrentDamage = ChangeValue * CoefDamage;

	if (Shield > 0.f)
	{
		ChangeShieldValue(CurrentDamage);
		if (Shield < 0)
		{

		}
	}
	else
	{
		Super::ChangeHealthValue_OnServer(ChangeValue);
	}
}

float UTPSCharacterHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UTPSCharacterHealthComponent::ChangeShieldValue(float ChangeValue)
{
	Shield += ChangeValue;
	ShieldChangeEvent_Multicast(Shield, ChangeValue);

	//OnShieldChange.Broadcast(Shield, ChangeValue);

	if (Shield > 100.f)
	{
		Shield = 100.f;
	}
	else
	{
		if (Shield < 0.f)
		{
			Shield = 0.f;
		}
	}

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CooldownShieldTimer, this, &UTPSCharacterHealthComponent::CooldownShieldEnd, CooldownShieldRecoveryTime, false);
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}


}

void UTPSCharacterHealthComponent::CooldownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this, &UTPSCharacterHealthComponent::RecoveryShield, ShieldRecoveryRate, true);
	}
}

void UTPSCharacterHealthComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp = tmp + ShieldRecoveryValue;
	if (tmp > 100.f)
	{
		Shield = 100.f;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		}
	}
	else
	{
		Shield = tmp;
	}

	ShieldChangeEvent_Multicast(Shield, ShieldRecoveryValue);
	
}

float UTPSCharacterHealthComponent::GetShieldValue()
{
	return Shield;
}

void UTPSCharacterHealthComponent::ShieldChangeEvent_Multicast_Implementation(float newShield, float newShieldRecoveryValue)
{
	OnShieldChange.Broadcast(newShield, newShieldRecoveryValue);
}

// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TPS/FuncLibrary/Types.h"
#include "TPS/Weapons/WeaponDefault.h"
#include "TPS/Character/TPSInventoryComponent.h"
#include "TPS/Character/TPSCharacterHealthComponent.h"
#include "TPS/Interface/TPS_IGameActor.h"
#include "TPS/Weapons/TPS_StateEffect.h"

#include "TPSCharacter.generated.h"

UCLASS(Blueprintable)
class ATPSCharacter : public ACharacter, public ITPS_IGameActor
{
	GENERATED_BODY()
protected:
	bool ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags) override;
	virtual void BeginPlay() override;

public:
	ATPSCharacter();

	FTimerHandle TimerHandle_RagDollTimer;

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponentTPS) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	////Now it's down there at Cursor section
	/** Returns CursorToWorld subobject **/
	//FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UTPSInventoryComponent* InventoryComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UTPSCharacterHealthComponent* HealthComponent;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	//class UDecalComponent* CursorToWorld;

public:
	//Cursor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		FVector CursorSize = FVector(20.f, 40.f, 40.f);
	UDecalComponent* CurrentCursor = nullptr;

	//Movement
	//to make "Replicated" work there should be GetLifetimeReplicatedProps overloaded
		//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	UPROPERTY(Replicated, BlueprintReadWrite)
	EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	FCharacterSpeed MovementSpeedInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool SprintRunEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool WalkEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool AimEnabled = false;

	//of live and dead
	/*UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool bIsAlive = true;*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		TArray<UAnimMontage*> DeadsAnim;

	//Ability
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
		TSubclassOf<UTPS_StateEffect> AbilityEffect;

	//Variables for smooth camera zoom
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera Zoom")
		float CameraZoomMax  = 2200.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera Zoom")
		float CameraZoomMin = 800.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera Zoom")
		float CameraZoomSpeed = -55.f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera Zoom")
		float CameraZoomDesiredDistance = 1800.f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera Zoom")
	float CameraZoomDistance = 1800.f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera Zoom")
	bool bCameraZoomActivated = false;

	//Weapon
	UPROPERTY(Replicated)
	AWeaponDefault* CurrentWeapon = nullptr;

	//Effects
	UPROPERTY(Replicated)
	TArray<UTPS_StateEffect*> Effects;
	UPROPERTY(ReplicatedUsing = EffectAdd_OnRep)
	UTPS_StateEffect* EffectAdd = nullptr;
	UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
	UTPS_StateEffect* EffectRemove = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	TArray<UParticleSystemComponent*> ParticleSystemEffects;

	//for demo 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
	FName InitWeaponName;

	//Inputs movement
	UFUNCTION()
	void InputAxisX(float Value);
	UFUNCTION()
	void InputAxisY(float Value);

	//Inputs weapon
	UFUNCTION()
	void InputAttackPressed();
	UFUNCTION()
	void InputAttackReleased();

	void HotkeyPressed(int32 HotkeyNum);
	DECLARE_DELEGATE_OneParam(FHotkeyPressed, int32);

	float AxisX = 0.f;
	float AxisY = 0.f;
	
	//Tick function
	void MovementTick(float DeltaTime);

	//Tick function for smooth camera zoom
	void CameraZoomTick(float DeltaTime);

	UFUNCTION(BlueprintCallable)
	void CameraZoom(float AxisValue);

	//Weapon functions
	UFUNCTION(BlueprintCallable)
	void AttackCharEvent(bool bIsFiring);
	UFUNCTION(BlueprintCallable)
	AWeaponDefault* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable)
	void InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);
	UFUNCTION(BlueprintCallable)
		void TryReloadWeapon(); 
	UFUNCTION()
		void WeaponReloadStart(UAnimMontage* Anim); 
	UFUNCTION()
		void WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadStart_BP(UAnimMontage* Anim); 
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadEnd_BP(bool bIsSuccess);

	//Movement functions
	UFUNCTION(BlueprintCallable)
	void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
	void ChangeMovementState();

	//Fire functions
	UFUNCTION()
		void WeaponFireStart(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponFireStart_BP(UAnimMontage* Anim);

	//Draw Decal Cursor function
	UFUNCTION(BlueprintCallable)
	UDecalComponent* GetCursorToWorld();

	//Inventory Func
	void TrySwicthNextWeapon();
	void TrySwitchPreviosWeapon();
	UFUNCTION(Server, Reliable)
	void TrySwitchWeaponToIndexByKeyInput_OnServer(int32 ToIndex);
	void DropCurrentWeapon();

	//Ability Func
	void TryAbilityEnabled();

	UPROPERTY(Replicated, BlueprintReadOnly, EditDefaultsOnly)
		int32 CurrentIndexWeapon = 0;

	//Interface
	EPhysicalSurface GetSurfaceType() override;
	UFUNCTION(BlueprintCallable)
	TArray<UTPS_StateEffect*> GetAllCurrerntEffects() override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void RemoveEffect(UTPS_StateEffect* RemoveEffect);
	void RemoveEffect_Implementation(UTPS_StateEffect* RemoveEffect) override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void AddEffect(UTPS_StateEffect* newEffect) ;
	void AddEffect_Implementation(UTPS_StateEffect* newEffect) override;

		FName GetEffectBone() override;
		FVector GetEffectOffset() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interface")
		FName EffectBoneName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interface", Meta = (MakeEditWidget = true))
		FVector EffectOffset;

	///DropWeapon Interface
	void DropWeaponToWorld_Implementation(FDropItem DropItemInfo) override;
	void DropAmmoToWorld_Implementation(EWeaponType TypeAmmo, int32 Count) override;


	//End of Interface

	UFUNCTION()
	void CharDead();
	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool GetIsAlive();
	UFUNCTION(NetMulticast, Reliable)
	void EnableRagdoll_Multicast();

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

	UFUNCTION(BlueprintNativeEvent)
	void CharDead_BP();

	//Network movement functions
	UFUNCTION(Server, Unreliable)
	void SetActorRotationByYaw_OnServer(float Yaw);
	UFUNCTION(NetMulticast, Unreliable)
	void SetActorRotationByYaw_Multicast(float Yaw);

	UFUNCTION(Server, Reliable)
		void SetMovementState_OnServer(EMovementState NewState);
	UFUNCTION(NetMulticast, Reliable)
		void SetMovementState_Multicast(EMovementState NewState);
	UFUNCTION(Server, Reliable)
		void TryReloadWeapon_OnServer();
	UFUNCTION(NetMulticast, Reliable)
		void PlayAnim_Multicast(UAnimMontage* Anim);

	UFUNCTION()
	void EffectAdd_OnRep();
	UFUNCTION()
	void EffectRemove_OnRep();

	UFUNCTION()
	void SwitchEffect(UTPS_StateEffect* Effect, bool bIsAdd);

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

};


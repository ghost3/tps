// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TPS/Interface/TPS_IGameActor.h"
#include "TPS/Weapons/TPS_StateEffect.h"

#include "TPS_EnvironmentStrructure.generated.h"

UCLASS()
class TPS_API ATPS_EnvironmentStrructure : public AActor, public ITPS_IGameActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATPS_EnvironmentStrructure();

protected:
	// Called when the game starts or when spawned

	virtual void BeginPlay() override;
	//bool ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags) override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	EPhysicalSurface GetSurfaceType() override;

	//Effects
	TArray<UTPS_StateEffect*> GetAllCurrerntEffects() override;
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void RemoveEffect(UTPS_StateEffect* RemoveEffect);
	void RemoveEffect_Implementation(UTPS_StateEffect* RemoveEffect) override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void AddEffect(UTPS_StateEffect* newEffect);
	void AddEffect_Implementation(UTPS_StateEffect* newEffect) override;

	FName GetEffectBone() override;
	FVector GetEffectOffset() override;

	///DropWeapon Interface
	void DropWeaponToWorld_Implementation(FDropItem DropItemInfo) override;
	void DropAmmoToWorld_Implementation(EWeaponType TypeAmmo, int32 Count) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interface")
		FName EffectBoneName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interface", Meta = (MakeEditWidget = true))
		FVector EffectOffset;

	//Effects
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Settings")
		TArray<UTPS_StateEffect*> Effects;
	UPROPERTY(ReplicatedUsing = EffectAdd_OnRep)
		UTPS_StateEffect* EffectAdd = nullptr;
	UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
		UTPS_StateEffect* EffectRemove = nullptr;

	UFUNCTION()
		void EffectAdd_OnRep();
	UFUNCTION()
		void EffectRemove_OnRep();
	UFUNCTION()
		void SwitchEffect(UTPS_StateEffect* Effect, bool bIsAdd);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		TArray<UParticleSystemComponent*> ParticleSystemEffects;

	UPROPERTY()
		FVector OffsetEffect = FVector(0);

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

};

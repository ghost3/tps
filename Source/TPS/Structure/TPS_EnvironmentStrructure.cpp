// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS_EnvironmentStrructure.h"
#include "Materials/MaterialInterface.h"
#include  "PhysicalMaterials/PhysicalMaterial.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "Engine/ActorChannel.h"

// Sets default values
ATPS_EnvironmentStrructure::ATPS_EnvironmentStrructure()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetReplicates(true);

}

// Called when the game starts or when spawned
void ATPS_EnvironmentStrructure::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATPS_EnvironmentStrructure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

EPhysicalSurface ATPS_EnvironmentStrructure::GetSurfaceType()
{

	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;

	UStaticMeshComponent* myMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMesh::StaticClass()));

	if (myMesh)
	{
		UMaterialInterface* myMaterial = myMesh->GetMaterial(0);

		if (myMaterial)
		{
			Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
		}
	}

	return Result;
}

TArray<UTPS_StateEffect*> ATPS_EnvironmentStrructure::GetAllCurrerntEffects()
{
	return Effects;
}

void ATPS_EnvironmentStrructure::RemoveEffect_Implementation(UTPS_StateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);

	SwitchEffect(RemoveEffect, false);
	EffectRemove = RemoveEffect;
}


void ATPS_EnvironmentStrructure::AddEffect_Implementation(UTPS_StateEffect* newEffect)
{
	Effects.Add(newEffect);

	SwitchEffect(newEffect, true);
	EffectAdd = newEffect;
}

FName ATPS_EnvironmentStrructure::GetEffectBone()
{
	return EffectBoneName;
}

FVector ATPS_EnvironmentStrructure::GetEffectOffset()
{
	return EffectOffset;
}

void ATPS_EnvironmentStrructure::EffectAdd_OnRep()
{
	if (EffectAdd)
	{
		SwitchEffect(EffectAdd, true);
	}
}


void ATPS_EnvironmentStrructure::EffectRemove_OnRep()
{
	if (EffectRemove)
	{
		SwitchEffect(EffectRemove, false);
	}
}

void ATPS_EnvironmentStrructure::SwitchEffect(UTPS_StateEffect* Effect, bool bIsAdd)
{
	if (bIsAdd)
	{
		if (Effect && Effect->ParticleEffect)
		{
			FName NameBoneToAttached = NAME_None;
			FVector Loc = OffsetEffect;

			USceneComponent* mySceneComponent = GetRootComponent();
			if (mySceneComponent)
			{
				UParticleSystemComponent* newParticleSystem = UGameplayStatics::SpawnEmitterAttached(Effect->ParticleEffect, mySceneComponent, NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
				ParticleSystemEffects.Add(newParticleSystem);
			}
		}
	}
	else
	{
		int32 i = 0;
		bool bIsFind = false;
		while (i < ParticleSystemEffects.Num() && !bIsFind)
		{
			if (ParticleSystemEffects[i]->Template && Effect->ParticleEffect && Effect->ParticleEffect == ParticleSystemEffects[i]->Template)
			{
				bIsFind = true;
				ParticleSystemEffects[i]->DeactivateSystem();
				ParticleSystemEffects[i]->DestroyComponent();
				ParticleSystemEffects.RemoveAt(i);


			}
			++i;
		}

	}
}

///DropWeapon Interface
void ATPS_EnvironmentStrructure::DropAmmoToWorld_Implementation(EWeaponType TypeAmmo, int32 Count)
{

}

void ATPS_EnvironmentStrructure::DropWeaponToWorld_Implementation(FDropItem DropItemInfo)
{

}

void ATPS_EnvironmentStrructure::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATPS_EnvironmentStrructure, Effects);
	DOREPLIFETIME(ATPS_EnvironmentStrructure, EffectAdd);
	DOREPLIFETIME(ATPS_EnvironmentStrructure, EffectRemove);


}
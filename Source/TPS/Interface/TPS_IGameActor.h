// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "TPS/Weapons/TPS_StateEffect.h"
#include "TPS/FuncLibrary/Types.h"

#include "TPS_IGameActor.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTPS_IGameActor : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TPS_API ITPS_IGameActor
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	//UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Event")
	//void AvaliableForEffectsBP();
	//UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Event")
	//bool AvaliableForEffects();

	virtual EPhysicalSurface GetSurfaceType();

	virtual TArray<UTPS_StateEffect*> GetAllCurrerntEffects();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void RemoveEffect(UTPS_StateEffect* RemoveEffect);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void AddEffect(UTPS_StateEffect* newEffect);

	virtual FName GetEffectBone();
	virtual FVector GetEffectOffset();

	///DropWeapon Interface
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void DropWeaponToWorld(FDropItem DropItemInfo);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void DropAmmoToWorld(EWeaponType TypeAmmo, int32 Count);
};
